# ACP2P Engineering Essentials
## Week 4 - Clean Code Practices
### Monster Battle APP (but without battles so far)

This is an application that is supposed to create monsters for battling (ideally), but for the purpose of the assignment, there are just some templates for creation of monsters (maybe it can be useful for RPG players):

To create a random monster a ```POST``` must be sent to:

```http://localhost:8080/v1/monster/create/random```

There is also a random Boss (a tough guy with lots of HP and stats)

```http://localhost:8080/v1/monster/create/boss/random```

To create a troll, a golem or a lizard:

```http://localhost:8080/v1/monster/create/troll (or lizard or golem)``` 

 They' will have some specific stats predefined in the ```Director``` class.

The response will be something like:

```
{
    "id": "1",
    "agility": 8,
    "baseAttack": 37,
    "baseDefense": 72,
    "buff": "REGEN_HP",
    "element": "ICE",
    "hp": 586,
    "monsterType": "TROLL",
    "movSpeed": 27,
    "skill": "DASH",
    "strength": 5,
    "boss": false
} 
```


#### The main focus of this week's assignment is to apply some clean code best practices, such as KISS, YAGNI and DRY - and also implement the project based on a Design Pattern.

 **YAGNI** This is a project that could've had thousands of features. I simplified things, once the only features available are those ones to create random monsters and a few types (e.g. Troll) with some presets. **Everything that is implemented is being used at some point.**

**KISS** Methods are short and have only one responsability.

**DRY** Util package was created to make repetitive calls within the packages for ease. Also, there are private methods inside classes (Service and Controller) for common actions (map a DTO or save an object to Repository).

**Design Pattern**

For the API RESTful Design, I chose to decouple as much as possible spliting the layers in CONTROLLER / SERVICE / REPOSITORY ones, and using a DTO in the Controller layer, mapping to an Entity before sending to the Service Layer.

Create a character in a game is fun because you can customize the various characteristics and attributes available, and then choose the ones that fits best your gameplay.

Due the complexity that a ```Monster``` have, I chose to implement the **Builder Pattern**, also using a ```Director ``` to make some presets and make things faster and less repetitive. This way, create a Monster make customizations is easy . The Pattern also makes it possible to use the Builder to create objects unrelated to Monster, but with the same behavior, like Hero, Champion, Character, NPC, or whatever is desired in a future implementation. This also contributes to the Single Responsibility Principle, where all the creating stuff is detached from the service layer.



