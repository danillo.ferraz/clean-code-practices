package com.avenuecode.monsterbattle;

import com.avenuecode.monsterbattle.builder.MonsterBuilder;
import com.avenuecode.monsterbattle.director.Director;
import org.modelmapper.ModelMapper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class Config {

    @Bean
    public Director director(){
        return new Director();
    }

    @Bean
    public MonsterBuilder monsterBuilder() {
        return new MonsterBuilder();
    }

    @Bean
    public ModelMapper modelMapper() {
        return new ModelMapper();
    }
}
