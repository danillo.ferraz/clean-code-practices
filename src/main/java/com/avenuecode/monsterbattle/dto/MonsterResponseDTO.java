package com.avenuecode.monsterbattle.dto;

import com.avenuecode.monsterbattle.model.Buff;
import com.avenuecode.monsterbattle.model.Element;
import com.avenuecode.monsterbattle.model.MonsterType;
import com.avenuecode.monsterbattle.model.Skill;

public class MonsterResponseDTO {

    private String id;

    private int agility;
    private int baseAttack;
    private int baseDefense;
    private Buff buff;
    private Element element;
    private int hp;
    private boolean isBoss;
    private MonsterType monsterType;
    private int movSpeed;
    private Skill skill;
    private int strength;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getStrength() {
        return strength;
    }

    public void setStrength(int strength) {
        this.strength = strength;
    }

    public int getAgility() {
        return agility;
    }

    public void setAgility(int agility) {
        this.agility = agility;
    }

    public int getBaseDefense() {
        return baseDefense;
    }

    public void setBaseDefense(int baseDefense) {
        this.baseDefense = baseDefense;
    }

    public int getHp() {
        return hp;
    }

    public void setHp(int hp) {
        this.hp = hp;
    }

    public int getMovSpeed() {
        return movSpeed;
    }

    public void setMovSpeed(int movSpeed) {
        this.movSpeed = movSpeed;
    }

    public int getBaseAttack() {
        return baseAttack;
    }

    public void setBaseAttack(int baseAttack) {
        this.baseAttack = baseAttack;
    }

    public Buff getBuff() {
        return buff;
    }

    public void setBuff(Buff buff) {
        this.buff = buff;
    }

    public Skill getSkill() {
        return skill;
    }

    public void setSkill(Skill skill) {
        this.skill = skill;
    }

    public Element getElement() {
        return element;
    }

    public void setElement(Element element) {
        this.element = element;
    }

    public MonsterType getMonsterType() {
        return monsterType;
    }

    public void setMonsterType(MonsterType monsterType) {
        this.monsterType = monsterType;
    }

    public void setBoss(boolean isBoss) {
        this.isBoss = isBoss;
    }

    public boolean isBoss() {
        return this.isBoss;
    }
}
