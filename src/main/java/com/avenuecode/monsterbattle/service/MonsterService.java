package com.avenuecode.monsterbattle.service;

import com.avenuecode.monsterbattle.builder.MonsterBuilder;
import com.avenuecode.monsterbattle.director.Director;
import com.avenuecode.monsterbattle.model.Monster;
import com.avenuecode.monsterbattle.repository.MonsterRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class MonsterService {

    @Autowired
    MonsterRepository repository;

    @Autowired
    MonsterBuilder builder;

    @Autowired
    Director director;

    public Monster createRandomMonster() {

        director.createRandomMonster(builder);
        return saveMonster();
    }

    public Monster createLizard() {

        director.createLizard(builder);
        return saveMonster();
    }

    public Monster createGolem() {

        director.createGolem(builder);
        return saveMonster();
    }

    public Monster createRandomBoss() {
        director.createRandomBoss(builder);
        return saveMonster();
    }

    public Monster createTroll() {
        director.createTroll(builder);
        return saveMonster();
    }

    //DRY - saving a monster is the same for all types of monsters
    private Monster saveMonster() {
        Monster monster = builder.getResult();
        repository.save(monster);
        return monster;
    }

    public Optional<Monster> getMonster(String monsterId) {
        return repository.findById(monsterId);
    }
}
