package com.avenuecode.monsterbattle.builder;

import com.avenuecode.monsterbattle.model.*;

public class MonsterBuilder implements Builder {

    private Skill skill;
    private MonsterType monsterType;
    private int strength;
    private int hp;
    private int agility;
    private int baseAttack;
    private int baseDefense;
    private int movSpeed;
    private Buff buff;
    private Element element;
    private boolean isBoss;

    @Override
    public void setSkill(Skill skill) {
        this.skill = skill;
    }

    @Override
    public void setMonsterType(MonsterType monsterType) {
        this.monsterType = monsterType;
    }

    @Override
    public void setStrength(int strength) {
        this.strength = strength;
    }

    @Override
    public void setHP(int hp) {
        this.hp = hp;
    }

    @Override
    public void setAgility(int agility) {
        this.agility = agility;
    }

    @Override
    public void setBaseAttack(int baseAttack) {
        this.baseAttack = baseAttack;
    }

    @Override
    public void setBaseDefense(int baseDefense) {
        this.baseDefense = baseDefense;
    }

    @Override
    public void setMovementSpeed(int movSpeed) {
        this.movSpeed = movSpeed;
    }

    @Override
    public void setBuff(Buff buff) {
        this.buff = buff;
    }

    @Override
    public void setElement(Element element) {
        this.element = element;
    }

    @Override
    public void setBoss(boolean isBoss) {
        this.isBoss = isBoss;
    }

    public Monster getResult() {
        return new Monster(
                strength,
                agility,
                baseDefense,
                hp,
                movSpeed,
                baseAttack,
                buff,
                skill,
                element,
                monsterType,
                isBoss);
    }
}
