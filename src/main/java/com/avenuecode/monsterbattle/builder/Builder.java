package com.avenuecode.monsterbattle.builder;

import com.avenuecode.monsterbattle.model.Buff;
import com.avenuecode.monsterbattle.model.Element;
import com.avenuecode.monsterbattle.model.MonsterType;
import com.avenuecode.monsterbattle.model.Skill;

public interface Builder {

    void setSkill(Skill skill);
    void setMonsterType(MonsterType monsterType);
    void setStrength(int strength);
    void setHP(int hp);
    void setAgility(int agility);
    void setBaseAttack(int baseAttack);
    void setBaseDefense(int baseDefense);
    void setMovementSpeed(int movSpeed);
    void setBuff(Buff buff);
    void setElement(Element element);
    void setBoss(boolean isBoss);

}
