package com.avenuecode.monsterbattle.repository;

import com.avenuecode.monsterbattle.model.Monster;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MonsterRepository extends JpaRepository<Monster, String> {
}
