package com.avenuecode.monsterbattle.director;

import com.avenuecode.monsterbattle.builder.Builder;
import com.avenuecode.monsterbattle.model.Buff;
import com.avenuecode.monsterbattle.model.Element;
import com.avenuecode.monsterbattle.model.MonsterType;
import com.avenuecode.monsterbattle.model.Skill;

import static com.avenuecode.monsterbattle.util.Constants.BOSS_MULTIPLIER;
import static com.avenuecode.monsterbattle.util.RandomUtils.*;


public class Director {

    public void createRandomBoss(Builder builder) {

        builder.setBoss(true);

        builder.setAgility(randomizeBetweenValues(50, 100));
        builder.setBaseAttack(randomizeAttack()*BOSS_MULTIPLIER);
        builder.setBaseDefense(randomizeDefense()*BOSS_MULTIPLIER);
        builder.setBuff(randomizeBuff());
        builder.setElement(randomizeElement());
        builder.setHP(randomizeBetweenValues(5000, 10000));
        builder.setMonsterType(randomizeMonsterType());
        builder.setMovementSpeed(randomizeBetweenValues(200, 300));
        builder.setSkill(randomizeSkill());
        builder.setStrength(randomizeBetweenValues(50, 150));
    }
    public void createRandomMonster(Builder builder) {

        builder.setBoss(false);

        builder.setAgility(randomizeBetweenValues(5, 10));
        builder.setBaseAttack(randomizeAttack());
        builder.setBaseDefense(randomizeDefense());
        builder.setBuff(randomizeBuff());
        builder.setElement(randomizeElement());
        builder.setHP(randomizeBetweenValues(500, 1000));
        builder.setMonsterType(randomizeMonsterType());
        builder.setMovementSpeed(randomizeBetweenValues(10, 100));
        builder.setSkill(randomizeSkill());
        builder.setStrength(randomizeBetweenValues(5, 10));
    }

    public void createLizard(Builder builder) {
        /*
         lizards commonly have the ability to become invisible, more agility
         and hp regen
         */
        this.createRandomMonster(builder);
        builder.setAgility(randomizeBetweenValues(10, 20));
        builder.setMonsterType(MonsterType.LIZARD);
        builder.setSkill(Skill.INVISIBILITY);
        builder.setBuff(Buff.REGEN_HP);
    }

    public void createGolem(Builder builder) {
        /*
         golems commonly have the ability to stun, have more strength
         and lots of hp
         */
        this.createRandomMonster(builder); //all other stuff are random
        builder.setMonsterType(MonsterType.GOLEM);
        builder.setSkill(Skill.STUN);
        builder.setStrength(randomizeBetweenValues(50, 60));
        builder.setHP(randomizeBetweenValues(1000, 2000));
    }

    public void createTroll(Builder builder) {
        /*
         trolls commonly have the ability to dash, their element is fire
         and their movement speed is high
         */
        this.createRandomMonster(builder);
        builder.setMonsterType(MonsterType.TROLL);
        builder.setSkill(Skill.DASH);
        builder.setElement(Element.FIRE);
        builder.setMovementSpeed(randomizeBetweenValues(100, 150));

    }
}
