package com.avenuecode.monsterbattle.controller;

import com.avenuecode.monsterbattle.dto.MonsterResponseDTO;
import com.avenuecode.monsterbattle.model.Monster;
import com.avenuecode.monsterbattle.service.MonsterService;
import org.apache.coyote.Response;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
public class MonsterController {

    @Autowired
    MonsterService monsterService;

    @Autowired
    ModelMapper modelMapper;


    @PostMapping("/v1/monster/create/random")
    public ResponseEntity<MonsterResponseDTO> createRandomMonster() {
        Monster monster = monsterService.createRandomMonster();
        return mapToResponseEntityMonsterResponseDTO(monster, HttpStatus.CREATED);
    }

    @PostMapping("/v1/monster/create/lizard")
    public ResponseEntity<MonsterResponseDTO> createLizard() {
        Monster monster = monsterService.createLizard();
        return mapToResponseEntityMonsterResponseDTO(monster, HttpStatus.CREATED);
    }

    @PostMapping("/v1/monster/create/golem")
    public ResponseEntity<MonsterResponseDTO> createGolem() {
        Monster monster = monsterService.createGolem();
        return mapToResponseEntityMonsterResponseDTO(monster, HttpStatus.CREATED);
    }

    @PostMapping("/v1/monster/create/troll")
    public ResponseEntity<MonsterResponseDTO> createTroll() {
        Monster monster = monsterService.createTroll();

        return mapToResponseEntityMonsterResponseDTO(monster, HttpStatus.CREATED);
    }

    @PostMapping("/v1/monster/create/boss/random")
    public ResponseEntity<MonsterResponseDTO> createRandomBoss() {
        Monster monster = monsterService.createRandomBoss();
        return mapToResponseEntityMonsterResponseDTO(monster, HttpStatus.CREATED);
    }

    //DRY - all endpoints that returns a MonsterResponseDTO should map to the same entity Monster
    private ResponseEntity<MonsterResponseDTO> mapToResponseEntityMonsterResponseDTO(Monster monster, HttpStatus httpStatus) {
        MonsterResponseDTO response = modelMapper.map(monster, MonsterResponseDTO.class);
        return ResponseEntity.status(httpStatus).body(response);
    }

    @GetMapping("v1/monster/{id}")
    public ResponseEntity<MonsterResponseDTO> getMonster(@PathVariable("id") String monsterId) {
        Optional<Monster> monster =  monsterService.getMonster(monsterId);
        if(monster.isPresent()) {
            return mapToResponseEntityMonsterResponseDTO(monster.get(), HttpStatus.OK);
        }
        return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
    }

}
