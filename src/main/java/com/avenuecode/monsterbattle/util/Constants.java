package com.avenuecode.monsterbattle.util;

public class Constants {

    public static final int MAX_BASE_ATTACK = 100;
    public static final int MIN_BASE_ATTACK = 10;
    public static final int MAX_BASE_DEFENSE = 90;
    public static final int MIN_BASE_DEFENSE= 5;
    public static final int BOSS_MULTIPLIER = 4;
}
