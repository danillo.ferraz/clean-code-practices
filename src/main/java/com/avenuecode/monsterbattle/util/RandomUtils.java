package com.avenuecode.monsterbattle.util;

import com.avenuecode.monsterbattle.model.Buff;
import com.avenuecode.monsterbattle.model.Element;
import com.avenuecode.monsterbattle.model.MonsterType;
import com.avenuecode.monsterbattle.model.Skill;

import java.util.Random;

import static com.avenuecode.monsterbattle.util.Constants.*;

public class RandomUtils {

    public static Element randomizeElement() {
        int pick = new Random().nextInt(Element.values().length);
        return Element.values()[pick];
    }

    public static MonsterType randomizeMonsterType() {
        int pick = new Random().nextInt(MonsterType.values().length);
        return MonsterType.values()[pick];
    }

    public static Skill randomizeSkill() {
        int pick = new Random().nextInt(Skill.values().length);
        return Skill.values()[pick];
    }

    public static Buff randomizeBuff() {
        int pick = new Random().nextInt(Buff.values().length);
        return Buff.values()[pick];
    }

    public static int randomizeDefense() {
        return new Random().nextInt(MAX_BASE_DEFENSE + 1 - MIN_BASE_DEFENSE) + MIN_BASE_DEFENSE;
    }

    public static int randomizeAttack() {
        return new Random().nextInt(MAX_BASE_ATTACK + 1 - MIN_BASE_ATTACK) + MIN_BASE_ATTACK;
    }

    public static int randomizeBetweenValues(int min, int max) {
        return new Random().nextInt(max + 1 - min) + min;
    }


}
