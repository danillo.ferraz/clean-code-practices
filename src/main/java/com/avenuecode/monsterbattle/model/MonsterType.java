package com.avenuecode.monsterbattle.model;

public enum MonsterType {
    GOLEM, LIZARD, TROLL, RAVENANT
}
