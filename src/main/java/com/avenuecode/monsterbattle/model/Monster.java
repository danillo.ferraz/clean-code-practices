package com.avenuecode.monsterbattle.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Monster {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private String id;

    private int strength;
    private int agility;
    private int baseDefense;
    private int hp;
    private int movSpeed;
    private int baseAttack;
    private Buff buff;
    private Skill skill;
    private Element element;
    private MonsterType monsterType;
    private boolean isBoss;

    public Monster() {
    }

    public Monster(int strength,
                   int agility,
                   int baseDefense,
                   int hp,
                   int movSpeed,
                   int baseAttack,
                   Buff buff,
                   Skill skill,
                   Element element,
                   MonsterType monsterType,
                   boolean isBoss) {

        this.strength = strength;
        this.agility = agility;
        this.baseDefense = baseDefense;
        this.hp = hp;
        this.movSpeed = movSpeed;
        this.baseAttack = baseAttack;
        this.buff = buff;
        this.skill = skill;
        this.element = element;
        this.monsterType = monsterType;
        this.isBoss = isBoss;
    }

    public boolean isBoss() {
        return isBoss;
    }

    public String getId() {
        return id;
    }

    public int getStrength() {
        return strength;
    }

    public int getAgility() {
        return agility;
    }

    public int getBaseDefense() {
        return baseDefense;
    }

    public int getHp() {
        return hp;
    }

    public int getMovSpeed() {
        return movSpeed;
    }

    public int getBaseAttack() {
        return baseAttack;
    }

    public Buff getBuff() {
        return buff;
    }

    public Skill getSkill() {
        return skill;
    }

    public Element getElement() {
        return element;
    }

    public MonsterType getMonsterType() {
        return monsterType;
    }
}
