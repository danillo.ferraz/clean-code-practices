package com.avenuecode.monsterbattle.model;

public enum Element {
    FIRE, ICE, WIND, EARTH, DARKNESS;
}
