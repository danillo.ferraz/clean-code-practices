package com.avenuecode.monsterbattle.model;

public enum Buff {
    REGEN_HP, INCREASE_SPEED, INCREASE_ATTACK, INCREASE_DEFENSE
}
